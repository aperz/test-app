AWS server setup
================================================================================


Important files
--------------------------------------------------------------------------------

##### test-app.ini

This is a configuration file for uWSGI.
It stays in the `test-app` directory.

##### test-app.service

This is the systemd file that creates a service entry. 
Goes to `/etc/systemd/system/`.

##### test-app.com.conf

This is the NGINX configuration file. It goes to `/etc/nginx/sites-available/`
and is linked to `/etc/nginx/sites-enabled/`.
The file name customarily reflects the domain (e.g. publicationqc.com.conf)

Setup
--------------------------------------------------------------------------------

1. In the AWS Console create a Security Group with the following rules:

Inbound rules:

| Type        | Protocol    | Port range  | Source              |
| :---------- | :---------- | :---------- | :------------------ |
| HTTP        | TCP         | 80          | 0.0.0.0/0           |
| HTTP        | TCP         | 80          | ::/0                |
| SSH         | TCP         | 22          | 0.0.0.0/0           |
| HTTPS       | TCP         | 443         | 0.0.0.0/0           |
| HTTPS       | TCP         | 443         | ::/0                |
| SSH         | TCP         | 22          | your IP/your subnet |

Outbound rules:

| Type        | Protocol    | Port range  | Source              |
| :---------- | :---------- | :---------- | :------------------ |
| All traffic | All         | All         | 0.0.0.0/0           |

2. In the AWS Console, create a new EC2 instance and assign it to the Security Group.
Create a key pair or link an existing key pair.
Then ssh into the instance (`ssh -i key.pem ubuntu@ec2...compute.amazonaws.com`)

3. Install packages

```bash
sudo apt update
sudo apt upgrade
sudo apt install nginx uwsgi virtualenv
```

4. Clone the test-app repo

```bash
cd
git clone https://gitlab.com/aperz/test-app
cd test-app
```

5. Set up the test-app environment

```bash
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
deactivate
```

6. Create a systemd entry for test-app

```bash
sudo cp test-app.service /etc/systemd/system/
```

7. Start and enable the service. See if it's working OK

```bash
sudo systemctl start test-app
sudo systemctl enable test-app
systemctl status test-app
```

8. Create the NGINX entry for test-app and test the configuration

```bash
sudo cp test-app.com.conf /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/test-app.com.conf /etc/nginx/sites-enabled
sudo rm /etc/nginx/sites-enabled/default
sudo nginx -t 
```

9. Update ufw rules. Start NGINX and see if it's working correctly

```bash
sudo ufw allow 'Nginx Full'
systemctl start nginx
systemctl status nginx
```

10. Set up the SSL certificate (for more detail, see: https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx)

```bash
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --nginx
```

11. In your browser, go to the instance address (Public IPv4 DNS) to see if everything is working.

Notes and sources
--------------------------------------------------------------------------------

For security reasons, consider creating a user and a group specifically for the service.

For convenience, consider setting up automatic renewals for the SSL cerificate with certbot.

Using a specific Python version in virtualenv:
https://hackersandslackers.com/multiple-versions-python-ubuntu/